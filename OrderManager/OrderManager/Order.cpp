#include "pch.h"
#include "Order.h"


Order::Order()
{
}


void Order::setOrderId(string id)
{
	this->orderId = id;
}

void Order::setCustomerId(string customerid)
{
	this->customerId = customerid;
}

void Order::setProductId(string productid)
{
	this->productId = productid;
}

void Order::setOrderDateTime(string datetime)
{
	this->orderDateTime = datetime;
}

string Order::getOrderid()
{
	return this->orderId;
}

string Order::getCustomerId()
{
	return this->customerId;
}

string Order::getProductId()
{
	return this->productId;
}

string Order::getorderDateTime()
{
	return this->orderDateTime;
}

