#include "pch.h"
#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <time.h>
#include "SQLManager.h"
#include "Customer.h"
#include "Product.h"
#include "Order.h"
#pragma warning(disable : 4996) // Disable C4996 warning c_str()
using namespace std;
#pragma once

class Manager
{
public:
	Manager();

	//CUSTOMERS
	bool displayCustomers(); //Customer View
	void addCustomer(); //Add
	void updateCustomer(Customer); //Update
	void customerDetails(Customer); //Details
	void deleteCustomer(Customer); //Delete
	vector<Customer> getCustomers(string); // Returns and displays list of customers

	//PRODUCTS
	bool displayProducts(); //Product View
	void addProduct(); //Add
	void updateProduct(Product); //Update
	void productDetails(Product); //Details
	void deleteProduct(Product); //Delete
	vector<Product> getProducts(string);  // Returns and displays list of products
	vector<Product> getAvaiableProducts(string); // Returns and displays list of products where quantity > 0

	//ORDERS
	bool displayOrders(); //Order View
	Order buildOrder(Customer, Product); //Creates Order line.
	void createOrder(); //Business logic for Order (gets components, adjusts stock) 
	void orderDetails(Order); //Details 
	vector<Order> getOrders(string); // Returns and displays list of orders

	void header(string, int); //Displays formatted headder of each view.
};

