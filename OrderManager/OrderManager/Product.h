#pragma once
#include <iostream>
#include <windows.h>
#include <string>
using namespace std;

class Product
{
public:
	//Cunstructors
	Product();
	Product(string);
	Product(string, string, double);
	Product(string, string, string, double);

	//Setters and Getters
	void setProductId(string);
	string getProductId();
	void setSku(string);
	string getSku();
	void setName(string);
	string getName();
	void setQuantity(int);
	int getQuantity();
	void setPrice(float);
	float getPrice();

private: 
	//Variables / atributes
	string productId;
	string sku;
	string name;
	int quantity;
	float price;
};

