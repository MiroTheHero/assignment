#include "pch.h"
#include "Product.h"


Product::Product()
{
}

Product::Product(string id)
{
	this->productId = id;
}

Product::Product(string sku, string name, double price)
{
	this->sku = sku;
	this->name = name;
	this->price = price;
}

Product::Product(string id, string sku, string name, double price)
{
	this->productId = id;
	this->sku = sku;
	this->name = name;
	this->price = price;
}

void Product::setProductId(string id)
{
	this->productId = id;
}

string Product::getProductId()
{
	return this->productId;
}

void Product::setSku(string sku)
{
	this->sku = sku;
}

string Product::getSku()
{
	return this->sku;
}

void Product::setName(string name)
{
	this->name = name;
}

string Product::getName()
{
	return this->name;
}

void Product::setQuantity(int quantity)
{
	this->quantity = quantity;
}

int Product::getQuantity()
{
	return this->quantity;
}

void Product::setPrice(float price)
{
	this->price = price;
}

float Product::getPrice()
{
	return this->price;
}

