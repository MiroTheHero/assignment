#pragma once
#include <iostream>
#include <windows.h>
#include <string>
using namespace std;

class OrderLine
{
public:
	OrderLine();
	OrderLine(string, string, int, float, float);
	
	void setLineId(string);
	void setProductId(string);
	void setOrderId(string);
	void setQuantity(int);
	void setUnitPrice(float);
	void setTotalPrice(float);

	string getLineId();
	string getProductId();
	string getOrderId();
	int getQuantity();
	float getUnitPrice();
	float getTotalPrice();

private: 
	string lineId;
	string orderId;
	string productId;
	int quantity;
	float unitPrice;
	float totalPrice;
};

