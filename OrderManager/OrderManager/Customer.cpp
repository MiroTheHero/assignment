#include "pch.h"
#include "Customer.h"



Customer::Customer()
{
}

Customer::Customer(string customerid)
{
	this->customerID = customerid;
}

Customer::Customer(string firstname, string lastname, string email, string address, string postcode, string city, string country)
{
	this->firstName = firstname;
	this->lastName = lastname;
	this->email = email;
	this->address = address;
	this->postCode = postcode;
	this->city = city;
	this->country = country;
}

Customer::Customer(string customerid, string firstname, string lastname, string email, string address, string postcode, string city, string country)
{
	this->customerID = customerid;
	this->firstName = firstname;
	this->lastName = lastname;
	this->email = email;
	this->address = address;
	this->postCode = postcode;
	this->city = city;
	this->country = country;
}

void Customer::setCustomerId(string customerid)
{
	this->customerID = customerid;
}

string Customer::getCustomerId()
{
	return this->customerID;
}

void Customer::setFirstName(string firstname)
{
	this->firstName = firstname;
}

string Customer::getFirstName()
{
	return this->firstName;
}

void Customer::setLastName(string lastname)
{
	this->lastName = lastname;
}

string Customer::getLastName()
{
	return this->lastName;
}

void Customer::setEmail(string email)
{
	this->email = email;
}

string Customer::getEmail()
{
	return this->email;
}

void Customer::setAddress(string address)
{
	this->address = address;
}

string Customer::getAddress()
{
	return this->address;
}

void Customer::setPostCode(string postcode)
{
	this->postCode = postcode;
}

string Customer::getPostCode()
{
	return this->postCode;
}

void Customer::setCity(string city)
{
	this->city = city;
}

string Customer::getCity()
{
	return this->city;
}

void Customer::setCountry(string country)
{
	this->country = country;
}

string Customer::getCountry()
{
	return this->country;
}


