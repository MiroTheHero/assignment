#include "pch.h"
#include <iostream>
#include <string>
#include "Manager.h"

using namespace std;


char mainMenu(Manager);


int main()
{
	Manager manager;
	char selection;
	bool flag = true;
	while (flag == true)
	{
		selection = mainMenu(manager);
		switch (selection)
		{
		case '0': {
			flag = manager.displayCustomers();
			break;
		}
		case '1': {
			flag = manager.displayProducts();
			break;
		}
		case '2': {
			flag = manager.displayOrders();
			break;
		}
		case '3': {
			flag = false;
			break;
		}
		default: {
			break;
		}
		}
	}
	return 0;
}

char mainMenu(Manager manager) {
	manager.header("MAIN MENU", 20);
	string menu[4] = { "Customer", "Products", "Orders", "Exit" };
	int arraySize = (sizeof(menu) / sizeof(*menu));
	for (int i = 0; i < arraySize; i++)
	{
		cout << " " << i << " - " << menu[i] << endl;
	}
	cout << endl;
	char selection;
	cout << "Make selection: ";
	cin >> selection;
	return selection;
}