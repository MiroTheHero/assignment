#pragma once
#include <iostream>
#include <windows.h>
#include <string>
#include <vector>
using namespace std;

class Order
{
public:

	//Cunstructors
	Order();

	//Setters and Getters
	void setOrderId(string);
	void setCustomerId(string);
	void setProductId(string);
	void setOrderDateTime(string);
	string getOrderid();
	string getCustomerId();
	string getProductId();
	string getorderDateTime();


private: 
	//Variables / atributes
	string orderId;
	string customerId;
	string productId;
	string orderDateTime;
};

