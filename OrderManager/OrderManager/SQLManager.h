#pragma once
#include <iostream>
#include <windows.h>
#include <sql.h>
#include <sqltypes.h>
#include <sqlext.h>
#include <vector>
#include <string>
#include <stdio.h>
#include "Customer.h"
#include "Product.h"
#include "Order.h"

using namespace std;

class SQLManager
{
public:
	SQLManager();

	vector<Customer> getAllCustomers(); //Get all customers from SQL
	Customer getCustomer(string); //Get all customer with specific ID from SQL
	void updateItem(Customer); //update single Item in SQL
	void deleteItem(Customer); //delete single item in SQL
	void insertItem(Customer); //insert single item into SQL

	vector<Product> getAllProducts(); //Get all products from SQL
	vector<Product> getAvailableAllProducts(); //Get all products from SQL where qunatity > 0
	Product getProduct(string); //Get all product with specific ID from SQL
	void updateItem(Product); //update single Item in SQL (overloaded)
	void deleteItem(Product); //delete single item in SQL (overloaded)
	void insertItem(Product); //insert single item into SQL (overloaded)

	vector<Order> getAllOrders(); //Get all orders from SQL
	Order getOrder(string); //Get all order with specific ID from SQL
	void updateItem(Order); //update single Item in SQL (overloaded)
	void deleteItem(Order); //delete single item in SQL (overloaded)
	void insertItem(Order); //insert single item into SQL (overloaded)

private:
	SQLHANDLE SqlEnvHandle; //Enviromental Handle for SQL 
	SQLHANDLE SqlConnectionHandle; //Connection Handle for SQL 
	SQLHANDLE SqlStatementHandle; //State Handle for SQL 
	SQLRETURN reCode; //Return code value
	SQLCHAR retConString[1024]; //Connection String 

	void startConnection(); //Sets handle and establishes connection
	void stopConnection(); //Releases all handles. 
};

