#pragma once
#include <iostream>
#include <windows.h>
#include <string>
using namespace std;

class Customer
{
public:
	//Cunstructors
	Customer();
	Customer(string);
	Customer(string, string, string, string, string, string, string);
	Customer(string, string, string, string, string, string, string, string);

	//Setters and Getters
	void setCustomerId(string);
	string getCustomerId();
	void setFirstName(string);
	string getFirstName();
	void setLastName(string);
	string getLastName();
	void setEmail(string);
	string getEmail();
	void setAddress(string);
	string getAddress();
	void setPostCode(string);
	string getPostCode();
	void setCity(string);
	string getCity();
	void setCountry(string);
	string getCountry();

private:
	//Variables / atributes
	string customerID;
	string firstName;
	string lastName;
	string email;
	string address;
	string postCode;
	string city;
	string country;
};

