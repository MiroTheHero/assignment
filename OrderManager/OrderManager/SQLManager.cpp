#include "pch.h"
#include "SQLManager.h"
#include <string>
#pragma warning(disable : 4996)


SQLManager::SQLManager()
{

}

////// START OF CUSTOMER //////

vector<Customer> SQLManager::getAllCustomers()
{
	vector<Customer> customers;
	Customer customer;
	char temp[256];
	char SqlQuery[] = "SELECT * FROM Customers";

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	while (SQLFetch(SqlStatementHandle) == SQL_SUCCESS) {
		SQLGetData(SqlStatementHandle, 1, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setCustomerId(temp);
		SQLGetData(SqlStatementHandle, 2, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setFirstName(temp);
		SQLGetData(SqlStatementHandle, 3, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setLastName(temp);
		SQLGetData(SqlStatementHandle, 4, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setEmail(temp);
		SQLGetData(SqlStatementHandle, 5, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setAddress(temp);
		SQLGetData(SqlStatementHandle, 6, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setPostCode(temp);
		SQLGetData(SqlStatementHandle, 7, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setCity(temp);
		SQLGetData(SqlStatementHandle, 8, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setCountry(temp);
		customers.push_back(customer);
	}
	stopConnection();
	return customers;
}


Customer SQLManager::getCustomer(string customerId)
{
	Customer customer;
	char temp[256];
	string SqlQueryString = "SELECT * FROM Customers WHERE CustomerId ='" + customerId + "'";
	char *SqlQuery = const_cast<char*>(SqlQueryString.c_str());

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	while (SQLFetch(SqlStatementHandle) == SQL_SUCCESS) {
		SQLGetData(SqlStatementHandle, 1, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setCustomerId(temp);
		SQLGetData(SqlStatementHandle, 2, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setFirstName(temp);
		SQLGetData(SqlStatementHandle, 3, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setLastName(temp);
		SQLGetData(SqlStatementHandle, 4, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setEmail(temp);
		SQLGetData(SqlStatementHandle, 5, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setAddress(temp);
		SQLGetData(SqlStatementHandle, 6, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setPostCode(temp);
		SQLGetData(SqlStatementHandle, 7, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setCity(temp);
		SQLGetData(SqlStatementHandle, 8, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		customer.setCountry(temp);
	}
	stopConnection();
	return customer;
}

void SQLManager::updateItem(Customer customer)
{

	string SqlQueryString = "UPDATE Customers SET FirstName= '" + customer.getFirstName()
		+ "', LastName= '" + customer.getLastName()
		+ "', Email = '" + customer.getEmail()
		+ "', Address = '" + customer.getAddress()
		+ "', PostCode = '" + customer.getPostCode()
		+ "', City = '" + customer.getCity()
		+ "', Country = '" + customer.getCountry()
		+ "' WHERE CustomerId ='" + customer.getCustomerId() + "'";

	char *SqlQuery = const_cast<char*>(SqlQueryString.c_str());

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery,SQL_NTS);
	stopConnection();
}


void SQLManager::deleteItem(Customer customer)
{
	string SqlQueryString = "DELETE FROM Customers WHERE CustomerId ='" + customer.getCustomerId() + "'";
	char *SqlQuery = const_cast<char*>(SqlQueryString.c_str());

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	stopConnection();
}

void SQLManager::insertItem(Customer customer)
{
	string SqlQueryString = "INSERT INTO Customers (FirstName, LastName, Email, Address, PostCode, City, Country) VALUES ('" + 
		customer.getFirstName() + "','" +
		customer.getLastName() + "','" + 
		customer.getEmail() + "','" + 
		customer.getAddress() + "','" +
		customer.getPostCode() + "','" + 
		customer.getCity() + "','" + 
		customer.getCountry() + "')";
	char *SqlQuery = const_cast<char*>(SqlQueryString.c_str());

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	stopConnection();
}

////// END OF CUSTOMER //////


////// START OF PRODUCT //////
vector<Product> SQLManager::getAllProducts()
{
	vector<Product> products;
	Product product;
	char temp[256];
	int quantity;
	double price;
	char SqlQuery[] = "SELECT * FROM Products";

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	while (SQLFetch(SqlStatementHandle) == SQL_SUCCESS) {
		SQLGetData(SqlStatementHandle, 1, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		product.setProductId(temp);
		SQLGetData(SqlStatementHandle, 2, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		product.setSku(temp);
		SQLGetData(SqlStatementHandle, 3, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		product.setName(temp);
		SQLGetData(SqlStatementHandle, 4, SQL_C_DEFAULT, &quantity, sizeof(quantity), NULL);
		product.setQuantity(quantity);
		SQLGetData(SqlStatementHandle, 5, SQL_C_DEFAULT, &price, sizeof(price), NULL);
		product.setPrice(price);
		products.push_back(product);
	}
	stopConnection();
	return products;
}

vector<Product> SQLManager::getAvailableAllProducts()
{
	vector<Product> products;
	Product product;
	char temp[256];
	int quantity;
	double price;
	char SqlQuery[] = "SELECT * FROM Products WHERE [Quantity] > 0";

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	while (SQLFetch(SqlStatementHandle) == SQL_SUCCESS) {
		SQLGetData(SqlStatementHandle, 1, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		product.setProductId(temp);
		SQLGetData(SqlStatementHandle, 2, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		product.setSku(temp);
		SQLGetData(SqlStatementHandle, 3, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		product.setName(temp);
		SQLGetData(SqlStatementHandle, 4, SQL_C_DEFAULT, &quantity, sizeof(quantity), NULL);
		product.setQuantity(quantity);
		SQLGetData(SqlStatementHandle, 5, SQL_C_DEFAULT, &price, sizeof(price), NULL);
		product.setPrice(price);
		products.push_back(product);
	}
	stopConnection();
	return products;
}

Product SQLManager::getProduct(string productId)
{
	Product product;
	char temp[256];
	double price;
	int quantity;
	string SqlQueryString = "SELECT * FROM Products WHERE ProductId ='" + productId + "'";
	char *SqlQuery = const_cast<char*>(SqlQueryString.c_str());

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	while (SQLFetch(SqlStatementHandle) == SQL_SUCCESS) {
		SQLGetData(SqlStatementHandle, 1, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		product.setProductId(temp);
		SQLGetData(SqlStatementHandle, 2, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		product.setSku(temp);
		SQLGetData(SqlStatementHandle, 3, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		product.setName(temp);
		SQLGetData(SqlStatementHandle, 4, SQL_C_DEFAULT, &quantity, sizeof(quantity), NULL);
		product.setPrice(quantity);
		SQLGetData(SqlStatementHandle, 5, SQL_C_DEFAULT, &price, sizeof(price), NULL);
		product.setPrice(price);
	}
	stopConnection();
	return product;
}

void SQLManager::updateItem(Product product)
{
	char price[128];
	int quantity = product.getQuantity();
	string quantityStr = to_string(quantity);
	sprintf(price,"%.02f",product.getPrice());

	string SqlQueryString = "UPDATE Products SET SKU= '" + product.getSku()
		+ "', Name= '" + product.getName()
		+ "', Quantity= " + quantityStr
		+ ", Price = '" + price +"'"
		+ " WHERE ProductId = '" + product.getProductId() + "'";
	char *SqlQuery = const_cast<char*>(SqlQueryString.c_str());

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	stopConnection();
}

void SQLManager::deleteItem(Product product)
{
	string SqlQueryString = "DELETE FROM Products WHERE ProductId ='" + product.getProductId() + "'";
	char *SqlQuery = const_cast<char*>(SqlQueryString.c_str());

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	stopConnection();
}

void SQLManager::insertItem(Product product)
{
	char price[128];
	sprintf(price, "%.02f", product.getPrice());
	int quantity = product.getQuantity();
	string quantityStr = to_string(quantity);
	string SqlQueryString = "INSERT INTO Products (SKU, Name, Quantity, Price) VALUES ('" +
		product.getSku() + "','" +
		product.getName() + "'," +
		quantityStr + "," +
		price + ")";
	char *SqlQuery = const_cast<char*>(SqlQueryString.c_str());

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	stopConnection();
}

////// END OF PRODUCT //////


////// START OF ORDER //////

vector<Order> SQLManager::getAllOrders()
{
	vector<Order> orders;
	Order order;
	char temp[256];
	char SqlQuery[] = "SELECT * FROM Orders";

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	while (SQLFetch(SqlStatementHandle) == SQL_SUCCESS) {
		SQLGetData(SqlStatementHandle, 1, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		order.setOrderId(temp);
		SQLGetData(SqlStatementHandle, 2, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		order.setCustomerId(temp);
		SQLGetData(SqlStatementHandle, 3, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		order.setProductId(temp);
		SQLGetData(SqlStatementHandle, 4, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		order.setOrderDateTime(temp);
		orders.push_back(order);
	}
	stopConnection();
	return orders;
}

Order SQLManager::getOrder(string id)
{
	Order order;
	char temp[256];
	string SqlQueryString = "SELECT * FROM Orders WHERE OrderId = '" + id + "'";
	char *SqlQuery = const_cast<char*>(SqlQueryString.c_str());

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	while (SQLFetch(SqlStatementHandle) == SQL_SUCCESS) {
		SQLGetData(SqlStatementHandle, 1, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		order.setOrderId(temp);
		SQLGetData(SqlStatementHandle, 2, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		order.setCustomerId(temp);
		SQLGetData(SqlStatementHandle, 3, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		order.setProductId(temp);
		SQLGetData(SqlStatementHandle, 4, SQL_C_DEFAULT, &temp, sizeof(temp), NULL);
		order.setOrderDateTime(temp);
	}
	stopConnection();
	return order;
}

void SQLManager::updateItem(Order item)
{
	string SqlQueryString = "UPDATE Orders SET CustomerId= '" + item.getCustomerId()
	+ "', ProdutcId = '" + item.getorderDateTime()
	+ "', OrderDateTime = '" + item.getorderDateTime() 
	+ "' WHERE OrderId = '" + item.getOrderid() + "'";
	char *SqlQuery = const_cast<char*>(SqlQueryString.c_str());

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	stopConnection();
}

void SQLManager::deleteItem(Order item)
{
	string SqlQueryString = "DELETE FROM Orders WHERE OrderId ='" + item.getOrderid() + "'";
	char *SqlQuery = const_cast<char*>(SqlQueryString.c_str());

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	stopConnection();
}

void SQLManager::insertItem(Order item)
{
	string SqlQueryString = "INSERT INTO Orders (CustomerId, ProductId, OrderDateTime) VALUES ('" + item.getCustomerId()
		+ "','" + item.getProductId()
		+ "','" + item.getorderDateTime()+ "')";
	char *SqlQuery = const_cast<char*>(SqlQueryString.c_str());

	startConnection();
	SQLExecDirect(SqlStatementHandle, (SQLCHAR*)SqlQuery, SQL_NTS);
	stopConnection();
}

////// END OF ORDER //////

void SQLManager::startConnection()
{
	this->SqlEnvHandle = NULL;
	this->SqlConnectionHandle = NULL;
	this->SqlStatementHandle = NULL;
	this->reCode = 0;

	SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &SqlEnvHandle);
	SQLSetEnvAttr(SqlEnvHandle, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, 0);

	SQLAllocHandle(SQL_HANDLE_DBC, SqlEnvHandle, &SqlConnectionHandle);
	SQLSetConnectAttr(SqlConnectionHandle, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);

	SQLDriverConnect(SqlConnectionHandle, NULL, (SQLCHAR*)"DRIVER={SQL Server}; SERVER=80.229.28.168; DATABASE=AssignmentDb; UID=PublicUser; PWD=Password123", SQL_NTS, retConString, 1024, NULL, SQL_DRIVER_NOPROMPT);
	SQLAllocHandle(SQL_HANDLE_STMT, SqlConnectionHandle, &SqlStatementHandle);

}

void SQLManager::stopConnection()
{
	SQLFreeHandle(SQL_HANDLE_STMT, SqlStatementHandle);
	SQLDisconnect(SqlConnectionHandle);
	SQLFreeHandle(SQL_HANDLE_DBC, SqlConnectionHandle);
	SQLFreeHandle(SQL_HANDLE_ENV, SqlEnvHandle);
}

