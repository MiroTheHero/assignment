#include "pch.h"
#include "Manager.h"


Manager::Manager()
{
}


bool Manager::displayCustomers() {
	SQLManager sql;
	vector<Customer> customers = getCustomers("MAIN > CUSTOMERS");
	int i;
	cout << endl;
	char selection;
	cout << "Press - (D)etails/(E)dit/(A)dd/(R)emove/(B)ack: ";
	cin >> selection;
	switch (selection)
	{
	case 'A': {
		addCustomer();
		return true;
		break;
	}
	case 'a': {
		addCustomer();
		return true;
		break;
	}
	case 'B': {
		return true;//main();
		break;
	}
	case 'b': {
		return true; //main();
		break;
	}
	case 'E': {
		cout << "Select user number: ";
		cin >> i;
		updateCustomer(customers[i]);
		return true;
		break;
	}
	case 'e': {
		cout << "Select user number: ";
		cin >> i;
		updateCustomer(customers[i]);
		return true;
		break;
	}
	case 'D': {
		cout << "Select user number: ";
		cin >> i;
		customerDetails(customers[i]);
		return true;
		break;
	}
	case 'd': {
		cout << "Select user number: ";
		cin >> i;
		customerDetails(customers[i]);
		return true;
		break;
	}
	case 'R': {
		cout << "Select user number: ";
		cin >> i;
		deleteCustomer(customers[i]);
		return true;
		break;
	}
	case 'r': {
		cout << "Select user number: ";
		cin >> i;
		deleteCustomer(customers[i]);
		return true;
		break;
	}
	default:
		break;
	}
}

void Manager::addCustomer() {
	SQLManager sql;
	Customer customer;
	string temp = "";
	header("MAIN MENU > CUSTOMERS >> Add", 50);

	cout << "First Name: ";
	cin.ignore();
	getline(cin, temp);
	customer.setFirstName(temp);
	cout << "Last Name: ";
	getline(cin, temp);
	customer.setLastName(temp);
	cout << "Email: ";
	getline(cin, temp);
	customer.setEmail(temp);
	cout << "Address: ";
	getline(cin, temp);
	customer.setAddress(temp);
	cout << "Post Code: ";
	getline(cin, temp);
	customer.setPostCode(temp);
	cout << "City: ";
	getline(cin, temp);
	customer.setCity(temp);
	cout << "Country: ";
	getline(cin, temp);
	customer.setCountry(temp);

	cout << endl;
	char selection;
	cout << "Press - (S)ave / (C)ancel: ";
	cin >> selection;
	if (selection == 'B' || selection == 'b') displayCustomers();
	else if (selection == 'S' || selection == 's') {
		sql.insertItem(customer);
		displayCustomers();
	}
}

void Manager::updateCustomer(Customer customer) {
	SQLManager sql;
	string temp = "";

	header("MAIN MENU > CUSTOMERS >> EDIT ", 50);

	cout << "First Name: ";
	cin.ignore();
	getline(cin, temp);
	if (temp == "") customer.setFirstName(customer.getFirstName());
	else customer.setFirstName(temp);

	cout << "Last Name: ";
	getline(cin, temp);
	if (temp == "") customer.setLastName(customer.getLastName());
	else customer.setLastName(temp);

	cout << "Email: ";
	getline(cin, temp);
	if (temp == "") customer.setEmail(customer.getEmail());
	else customer.setEmail(temp);

	cout << "Address: ";
	getline(cin, temp);
	if (temp == "") customer.setAddress(customer.getAddress());
	else customer.setAddress(temp);


	cout << "Post Code: ";
	getline(cin, temp);
	if (temp == "") customer.setPostCode(customer.getPostCode());
	else customer.setPostCode(temp);

	cout << "City: ";
	getline(cin, temp);
	if (temp == "") customer.setCity(customer.getCity());
	else customer.setCity(temp);

	cout << "Country: ";
	getline(cin, temp);
	if (temp == "") customer.setCountry(customer.getCountry());
	else customer.setCountry(temp);

	cout << endl;
	char selection;
	cout << "Press - (S)ave / (C)ancel: ";
	cin >> selection;
	if (selection == 'C' || selection == 'c') displayCustomers();
	else if (selection == 'S' || selection == 's') {
		sql.updateItem(customer);
		displayCustomers();
	}
}

void Manager::customerDetails(Customer customer) {

	header("MAIN MENU > CUSTOMERS >> DETAILS", 50);

	cout << "CustomerId: " << customer.getCustomerId() << endl;
	cout << "First Name: " << customer.getFirstName() << endl;
	cout << "Last Name: " << customer.getLastName() << endl;
	cout << "Email: " << customer.getEmail() << endl;
	cout << "Address: " << customer.getAddress() << endl;
	cout << "Post Code: " << customer.getPostCode() << endl;
	cout << "City: " << customer.getCity() << endl;
	cout << "Country: " << customer.getCountry() << endl;

	cout << endl;
	char selection;
	cout << "Press (B)back: ";
	cin >> selection;
	if (selection == 'B' || selection == 'b') displayCustomers();
}

void Manager::deleteCustomer(Customer customer) {
	header("MAIN MENU > CUSTOMERS >> REMOVED", 50);
	SQLManager sql;
	sql.deleteItem(customer);
	cout << endl;
	char selection;
	cout << "Customer " << customer.getFirstName() << " " << customer.getLastName() << " removed" << endl;
	cout << endl;
	cout << "Press (B)ack: ";
	cin >> selection;
	if (selection == 'B' || selection == 'b') displayCustomers();
}

bool Manager::displayProducts() {
	SQLManager sql;
	vector<Product> products = getProducts("MAIN > PRODUCTS");
	int i;
	cout << endl;
	char selection;
	cout << "Press - (D)etails/(E)dit/(A)dd/(R)emove/(B)ack: ";
	cin >> selection;
	switch (selection)
	{
	case 'A': {
		addProduct();
		return true;
		break;
	}
	case 'a': {
		addProduct();
		return true;
		break;
	}
	case 'B': {
		return true; //main();
		break;
	}
	case 'b': {
		return true; //main();
		break;
	}
	case 'E': {
		cout << "Select product number: ";
		cin >> i;
		updateProduct(products[i]);
		return true;
		break;
	}
	case 'e': {
		cout << "Select product number: ";
		cin >> i;
		updateProduct(products[i]);
		return true;
		break;
	}
	case 'D': {
		cout << "Select product number: ";
		cin >> i;
		productDetails(products[i]);
		return true;
		break;
	}
	case 'd': {
		cout << "Select product number: ";
		cin >> i;
		productDetails(products[i]);
		return true;
		break;
	}
	case 'R': {
		cout << "Select product number: ";
		cin >> i;
		deleteProduct(products[i]);
		return true;
		break;
	}
	case 'r': {
		cout << "Select product number: ";
		cin >> i;
		deleteProduct(products[i]);
		return true;
		break;
	}
	default:
		return true;
		break;
	}
}

void Manager::addProduct() {
	SQLManager sql;
	Product product;
	string temp = "";
	int quantity;
	float price;
	header("MAIN MENU > PRODUCTS >> ADD", 50);

	cout << "SKU Name: ";
	cin.ignore();
	getline(cin, temp);
	product.setSku(temp);

	cout << "Product Name: ";
	getline(cin, temp);
	product.setName(temp);

	cout << "Quantity: ";
	cin >> quantity;
	product.setQuantity(quantity);

	cout << "Price: ";
	cin >> price;
	product.setPrice(price);

	cout << endl;
	char selection;
	cout << "Press - (S)ave / (C)ancel: ";
	cin >> selection;
	if (selection == 'C' || selection == 'c') displayProducts();
	else if (selection == 'S' || selection == 's') {
		sql.insertItem(product);
		displayProducts();
	}
};
void Manager::updateProduct(Product product) {

	SQLManager sql;
	string temp = "";
	int quantity;
	float price;
	header("MAIN MENU > PRODUCTS >> UPDATE", 50);

	cout << "SKU Name: ";
	cin.ignore();
	getline(cin, temp);
	if (temp == "") product.setSku(product.getSku());
	else product.setSku(temp);


	cout << "Product Name: ";
	getline(cin, temp);
	if (temp == "") product.setName(product.getName());
	else product.setName(temp);

	cout << "Quantity: ";
	cin >> quantity;
	if (quantity == NULL) product.setQuantity(product.getQuantity());
	else product.setQuantity(quantity);

	cout << "Price: ";
	cin >> price;
	if (price == NULL) product.setPrice(product.getPrice());
	else product.setPrice(price);

	cout << endl;
	char selection;
	cout << "Press - (S)ave / (C)ancel: ";
	cin >> selection;
	if (selection == 'C' || selection == 'c') displayProducts();
	else if (selection == 'S' || selection == 's') {
		sql.updateItem(product);
		displayProducts();
	}
};

void Manager::productDetails(Product product) {
	header("MAIN MENU > PRODUCTS >> DETAILS", 50);

	cout << "ProductId: " << product.getProductId() << endl;
	cout << "SKU: " << product.getSku() << endl;
	cout << "Product Name: " << product.getName() << endl;
	cout << "Quantity Available: " << product.getQuantity() << endl;
	cout << "Price: " << (char)156 << product.getPrice() << endl;

	cout << endl;
	char selection;
	cout << "Press (B)back: ";
	cin >> selection;
	if (selection == 'B' || selection == 'b') displayProducts();
};

void Manager::deleteProduct(Product product) {
	header("MAIN MENU > PRODUCTS >> REMOVED", 50);
	SQLManager sql;
	sql.deleteItem(product);
	cout << endl;
	char selection;
	cout << "Product " << product.getSku() << " - " << product.getName() << " removed" << endl;
	cout << endl;
	cout << "Press (B)ack: ";
	cin >> selection;
	if (selection == 'B' || selection == 'b') displayProducts();
};

bool Manager::displayOrders() {
	SQLManager sql;
	vector<Order> orders = getOrders("MAIN MENU > ORDERS");
	int i;
	cout << endl;
	char selection;
	cout << "Press - (D)etails/(A)dd/(B)ack: ";
	cin >> selection;
	switch (selection)
	{
	case 'A': {
		createOrder();
		return true;
		break;
	}
	case 'a': {
		createOrder();
		return true;
		break;
	}
	case 'B': {
		return true; //main();
		break;
	}
	case 'b': {
		return true; // main();
		break;
	}
	case 'D': {
		cout << "Select product number: ";
		cin >> i;
		orderDetails(orders[i]);
		return true;
		break;
	}
	case 'd': {
		cout << "Select product number: ";
		cin >> i;
		orderDetails(orders[i]);
		return true;
		break;
	}
	default:
		return true;
		break;
	}
}

void Manager::createOrder() {
	SQLManager sql;
	vector<Customer> customers;
	vector<Product> products;
	Customer customer;
	Product product;
	Order order;
	char selection;
	int i;
	header("MAIN > ORDERS >> CREATE ORDER", 70);

	//SELECT CUSTOMER
	cout << "Select customer" << endl << endl;
	customers = getCustomers("MAIN > ORDERS >> CREATE ORDER >>> SELECT CUSTOMER");
	cout << endl;
	cout << "Make selection: ";
	cin >> i;
	customer = customers[i];
	order.setCustomerId(customer.getCustomerId());

	//SELECT PRODUCT
	products = getAvaiableProducts("MAIN > ORDERS >> CREATE ORDER >>> SELECT PRODUCT");
	cout << endl;
	cout << "Add product to order or : ";
	cin >> i;
	product = products[i];

	//CREATE ORDER
	order = buildOrder(customer, product);

	//UPDATE STOCK
	product.setQuantity(product.getQuantity() - 1);
	sql.updateItem(product);

	//DISPLAY ORDER
	header("MAIN > ORDERS >> CREATE ORDER >>> ORDER CREATED", 70);

	cout << "Order date: " << order.getorderDateTime() << endl;
	cout << "Customer Name: " << customer.getFirstName() + " " + customer.getLastName() << endl;
	cout << "Product SKU: " << product.getSku() << endl;
	cout << "Product Name: " << product.getName() << endl;
	cout << "Quantity Available: " << 1 << endl;
	cout << "Price: " << (char)156 << product.getPrice() << endl;

	cout << endl;
	cout << "Press (B)back: ";
	cin >> selection;
	if (selection == 'B' || selection == 'b') displayOrders();
}

vector<Customer> Manager::getCustomers(string str) {
	SQLManager sql;
	vector<Customer> customers = sql.getAllCustomers();
	header(str, 70);
	for (int i = 0; i < customers.size(); i++)
	{
		cout << i << " - "
			<< customers[i].getFirstName() << " "
			<< customers[i].getLastName() << " ( "
			<< customers[i].getEmail() << " )"
			<< endl;
	}
	return customers;
}

vector<Product> Manager::getProducts(string str) {
	SQLManager sql;
	vector<Product> products = sql.getAllProducts();
	char price[128];
	header(str, 70);
	for (int i = 0; i < products.size(); i++)
	{
		sprintf(price, "%.02f", products[i].getPrice());
		cout << i << " - "
			<< products[i].getSku() << " - "
			<< products[i].getName() << " - Quantity: "
			<< products[i].getQuantity() << " - "
			<< (char)156 << price << endl;
	}
	return products;
}

vector<Order> Manager::getOrders(string str) {
	SQLManager sql;
	vector<Order> orders = sql.getAllOrders();
	Customer customer;
	Product product;
	vector<Product> products = sql.getAllProducts();
	header(str, 80);
	for (int i = 0; i < orders.size(); i++)
	{
		customer = sql.getCustomer(orders[i].getCustomerId());
		product = sql.getProduct(orders[i].getProductId());
		cout << i << " - "
			<< customer.getEmail() << " - "
			<< product.getName() << " - "
			<< orders[i].getorderDateTime() << endl;
	}
	return orders;
}
vector<Product> Manager::getAvaiableProducts(string str) {
	SQLManager sql;
	vector<Product> products = sql.getAvailableAllProducts();
	char price[128];
	header(str, 70);
	for (int i = 0; i < products.size(); i++)
	{
		sprintf(price, "%.02f", products[i].getPrice());
		cout << i << " - "
			<< products[i].getSku() << " - "
			<< products[i].getName() << " - Quantity: "
			<< products[i].getQuantity() << " - "
			<< (char)156 << price << endl;
	}
	return products;
}

Order Manager::buildOrder(Customer customer, Product product) {
	SQLManager sql;
	Order order;
	order.setCustomerId(customer.getCustomerId());
	order.setProductId(product.getProductId());
	time_t now = time(0);
	tm *ltm = localtime(&now);
	char time[128];
	sprintf(time, "%2d-%2d-%4d %2d:%2d:%2d", ltm->tm_mday, ltm->tm_mon + 1, ltm->tm_year + 1900, ltm->tm_hour, ltm->tm_min, ltm->tm_sec);
	order.setOrderDateTime(time);
	sql.insertItem(order);
	return order;
}

void Manager::header(string header, int size)
{
	system("cls");
	string line, spacer;
	for (int i = 0; i < (size - header.length()) / 2; i++)
	{
		spacer += " ";
	}
	string value = spacer + header + spacer;
	for (int i = 0; i < value.length(); i++)
	{
		line += "*";
	}
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 248);
	cout << value << endl;
	cout << line << endl;
	cout << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
}

void Manager::orderDetails(Order order) {
	SQLManager sql;
	Customer customer = sql.getCustomer(order.getCustomerId());
	Product product = sql.getProduct(order.getProductId());
	char selection;
	header("MAIN > ORDERS >> CREATE ORDER >>> ORDER DETAILS", 70);

	cout << "Order date: " << order.getorderDateTime() << endl;
	cout << "Customer Name: " << customer.getFirstName() + " " + customer.getLastName() << endl;
	cout << "Product SKU: " << product.getSku() << endl;
	cout << "Product Name: " << product.getName() << endl;
	cout << "Quantity Available: " << 1 << endl;
	cout << "Price: " << (char)156 << product.getPrice() << endl;

	cout << endl;
	cout << "Press (B)back: ";
	cin >> selection;
	if (selection == 'B' || selection == 'b') displayOrders();
}